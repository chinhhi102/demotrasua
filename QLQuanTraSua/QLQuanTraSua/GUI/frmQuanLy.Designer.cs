﻿namespace QLQuanTraSua.GUI
{
    partial class frmQuanLy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.bntQLTaiKhoan = new System.Windows.Forms.Button();
            this.bntQLBan = new System.Windows.Forms.Button();
            this.bntQLNhomMon = new System.Windows.Forms.Button();
            this.bntQLThucDon = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbSL = new System.Windows.Forms.Label();
            this.lbTongSo = new System.Windows.Forms.Label();
            this.cbbNhomMon = new System.Windows.Forms.ComboBox();
            this.bntXoa = new System.Windows.Forms.Button();
            this.bntSua = new System.Windows.Forms.Button();
            this.bntThem = new System.Windows.Forms.Button();
            this.dtgv = new System.Windows.Forms.DataGridView();
            this.txtTim = new System.Windows.Forms.TextBox();
            this.lbNhomMon = new System.Windows.Forms.Label();
            this.lbTim = new System.Windows.Forms.Label();
            this.lbTenButton = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.bntQLTaiKhoan);
            this.panel1.Controls.Add(this.bntQLBan);
            this.panel1.Controls.Add(this.bntQLNhomMon);
            this.panel1.Controls.Add(this.bntQLThucDon);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 476);
            this.panel1.TabIndex = 0;
            // 
            // bntQLTaiKhoan
            // 
            this.bntQLTaiKhoan.Location = new System.Drawing.Point(4, 175);
            this.bntQLTaiKhoan.Name = "bntQLTaiKhoan";
            this.bntQLTaiKhoan.Size = new System.Drawing.Size(191, 34);
            this.bntQLTaiKhoan.TabIndex = 3;
            this.bntQLTaiKhoan.Text = "Quản Lý Tài Khoản";
            this.bntQLTaiKhoan.UseVisualStyleBackColor = true;
            this.bntQLTaiKhoan.Click += new System.EventHandler(this.bntQLTaiKhoan_Click);
            // 
            // bntQLBan
            // 
            this.bntQLBan.Location = new System.Drawing.Point(4, 118);
            this.bntQLBan.Name = "bntQLBan";
            this.bntQLBan.Size = new System.Drawing.Size(191, 34);
            this.bntQLBan.TabIndex = 2;
            this.bntQLBan.Text = "Quản Lý Bàn";
            this.bntQLBan.UseVisualStyleBackColor = true;
            this.bntQLBan.Click += new System.EventHandler(this.bntQLBan_Click);
            // 
            // bntQLNhomMon
            // 
            this.bntQLNhomMon.Location = new System.Drawing.Point(4, 61);
            this.bntQLNhomMon.Name = "bntQLNhomMon";
            this.bntQLNhomMon.Size = new System.Drawing.Size(191, 34);
            this.bntQLNhomMon.TabIndex = 1;
            this.bntQLNhomMon.Text = "Quản Lý Nhóm Món";
            this.bntQLNhomMon.UseVisualStyleBackColor = true;
            this.bntQLNhomMon.Click += new System.EventHandler(this.bntQLNhomMon_Click);
            // 
            // bntQLThucDon
            // 
            this.bntQLThucDon.Location = new System.Drawing.Point(4, 4);
            this.bntQLThucDon.Name = "bntQLThucDon";
            this.bntQLThucDon.Size = new System.Drawing.Size(191, 34);
            this.bntQLThucDon.TabIndex = 0;
            this.bntQLThucDon.Text = "Quản Lý Thực Đơn";
            this.bntQLThucDon.UseVisualStyleBackColor = true;
            this.bntQLThucDon.Click += new System.EventHandler(this.bntQLThucDon_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbSL);
            this.panel2.Controls.Add(this.lbTongSo);
            this.panel2.Controls.Add(this.cbbNhomMon);
            this.panel2.Controls.Add(this.bntXoa);
            this.panel2.Controls.Add(this.bntSua);
            this.panel2.Controls.Add(this.bntThem);
            this.panel2.Controls.Add(this.dtgv);
            this.panel2.Controls.Add(this.txtTim);
            this.panel2.Controls.Add(this.lbNhomMon);
            this.panel2.Controls.Add(this.lbTim);
            this.panel2.Controls.Add(this.lbTenButton);
            this.panel2.Location = new System.Drawing.Point(235, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(783, 476);
            this.panel2.TabIndex = 1;
            // 
            // lbSL
            // 
            this.lbSL.AutoSize = true;
            this.lbSL.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSL.ForeColor = System.Drawing.Color.Red;
            this.lbSL.Location = new System.Drawing.Point(576, 439);
            this.lbSL.Name = "lbSL";
            this.lbSL.Size = new System.Drawing.Size(49, 15);
            this.lbSL.TabIndex = 6;
            this.lbSL.Text = "TongSo";
            // 
            // lbTongSo
            // 
            this.lbTongSo.AutoSize = true;
            this.lbTongSo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSo.ForeColor = System.Drawing.Color.Red;
            this.lbTongSo.Location = new System.Drawing.Point(468, 439);
            this.lbTongSo.Name = "lbTongSo";
            this.lbTongSo.Size = new System.Drawing.Size(49, 15);
            this.lbTongSo.TabIndex = 6;
            this.lbTongSo.Text = "TongSo";
            // 
            // cbbNhomMon
            // 
            this.cbbNhomMon.FormattingEnabled = true;
            this.cbbNhomMon.Location = new System.Drawing.Point(493, 62);
            this.cbbNhomMon.Name = "cbbNhomMon";
            this.cbbNhomMon.Size = new System.Drawing.Size(121, 21);
            this.cbbNhomMon.TabIndex = 1;
            this.cbbNhomMon.SelectedIndexChanged += new System.EventHandler(this.cbbNhomMon_SelectedIndexChanged);
            // 
            // bntXoa
            // 
            this.bntXoa.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntXoa.Location = new System.Drawing.Point(673, 217);
            this.bntXoa.Name = "bntXoa";
            this.bntXoa.Size = new System.Drawing.Size(75, 53);
            this.bntXoa.TabIndex = 4;
            this.bntXoa.Text = "Xóa";
            this.bntXoa.UseVisualStyleBackColor = true;
            this.bntXoa.Click += new System.EventHandler(this.bntXoa_Click);
            // 
            // bntSua
            // 
            this.bntSua.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntSua.Location = new System.Drawing.Point(673, 158);
            this.bntSua.Name = "bntSua";
            this.bntSua.Size = new System.Drawing.Size(75, 53);
            this.bntSua.TabIndex = 3;
            this.bntSua.Text = "Sửa";
            this.bntSua.UseVisualStyleBackColor = true;
            this.bntSua.Click += new System.EventHandler(this.bntSua_Click);
            // 
            // bntThem
            // 
            this.bntThem.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntThem.Location = new System.Drawing.Point(673, 99);
            this.bntThem.Name = "bntThem";
            this.bntThem.Size = new System.Drawing.Size(75, 53);
            this.bntThem.TabIndex = 2;
            this.bntThem.Text = "Thêm";
            this.bntThem.UseVisualStyleBackColor = true;
            this.bntThem.Click += new System.EventHandler(this.bntThem_Click);
            // 
            // dtgv
            // 
            this.dtgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgv.Location = new System.Drawing.Point(44, 99);
            this.dtgv.Name = "dtgv";
            this.dtgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgv.Size = new System.Drawing.Size(591, 333);
            this.dtgv.TabIndex = 5;
            this.dtgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgv_CellClick);
            // 
            // txtTim
            // 
            this.txtTim.Location = new System.Drawing.Point(164, 62);
            this.txtTim.Name = "txtTim";
            this.txtTim.Size = new System.Drawing.Size(145, 20);
            this.txtTim.TabIndex = 0;
            this.txtTim.TextChanged += new System.EventHandler(this.txtTim_TextChanged);
            // 
            // lbNhomMon
            // 
            this.lbNhomMon.AutoSize = true;
            this.lbNhomMon.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNhomMon.Location = new System.Drawing.Point(395, 61);
            this.lbNhomMon.Name = "lbNhomMon";
            this.lbNhomMon.Size = new System.Drawing.Size(80, 19);
            this.lbNhomMon.TabIndex = 1;
            this.lbNhomMon.Text = "Nhóm món:";
            // 
            // lbTim
            // 
            this.lbTim.AutoSize = true;
            this.lbTim.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTim.Location = new System.Drawing.Point(40, 61);
            this.lbTim.Name = "lbTim";
            this.lbTim.Size = new System.Drawing.Size(27, 19);
            this.lbTim.TabIndex = 1;
            this.lbTim.Text = "tim";
            // 
            // lbTenButton
            // 
            this.lbTenButton.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenButton.ForeColor = System.Drawing.Color.Red;
            this.lbTenButton.Location = new System.Drawing.Point(114, 15);
            this.lbTenButton.Name = "lbTenButton";
            this.lbTenButton.Size = new System.Drawing.Size(568, 41);
            this.lbTenButton.TabIndex = 0;
            this.lbTenButton.Text = "hi";
            this.lbTenButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmQuanLy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 500);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(140, 70);
            this.Name = "frmQuanLy";
            this.Text = "frmQuanLy";
            this.Activated += new System.EventHandler(this.frmQuanLy_Activated);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bntQLTaiKhoan;
        private System.Windows.Forms.Button bntQLBan;
        private System.Windows.Forms.Button bntQLNhomMon;
        private System.Windows.Forms.Button bntQLThucDon;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbbNhomMon;
        private System.Windows.Forms.Button bntXoa;
        private System.Windows.Forms.Button bntSua;
        private System.Windows.Forms.Button bntThem;
        private System.Windows.Forms.DataGridView dtgv;
        private System.Windows.Forms.TextBox txtTim;
        private System.Windows.Forms.Label lbNhomMon;
        private System.Windows.Forms.Label lbTim;
        private System.Windows.Forms.Label lbTenButton;
        private System.Windows.Forms.Label lbSL;
        private System.Windows.Forms.Label lbTongSo;
    }
}