﻿namespace QLQuanTraSua.GUI
{
    partial class frmTaiKhoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTen = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenTK = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMK = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbQuyen = new System.Windows.Forms.ComboBox();
            this.bntXacNhan = new System.Windows.Forms.Button();
            this.bntHuyBo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbTen
            // 
            this.lbTen.AutoSize = true;
            this.lbTen.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTen.ForeColor = System.Drawing.Color.Red;
            this.lbTen.Location = new System.Drawing.Point(75, 23);
            this.lbTen.Name = "lbTen";
            this.lbTen.Size = new System.Drawing.Size(49, 19);
            this.lbTen.TabIndex = 0;
            this.lbTen.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên tài khoản:";
            // 
            // txtTenTK
            // 
            this.txtTenTK.Location = new System.Drawing.Point(101, 67);
            this.txtTenTK.Name = "txtTenTK";
            this.txtTenTK.Size = new System.Drawing.Size(162, 20);
            this.txtTenTK.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(34, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mật khẩu:";
            // 
            // txtMK
            // 
            this.txtMK.Location = new System.Drawing.Point(101, 117);
            this.txtMK.Name = "txtMK";
            this.txtMK.Size = new System.Drawing.Size(162, 20);
            this.txtMK.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(49, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Quyền:";
            // 
            // cbbQuyen
            // 
            this.cbbQuyen.FormattingEnabled = true;
            this.cbbQuyen.Items.AddRange(new object[] {
            "Quản Lý",
            "Nhân viên"});
            this.cbbQuyen.Location = new System.Drawing.Point(101, 168);
            this.cbbQuyen.Name = "cbbQuyen";
            this.cbbQuyen.Size = new System.Drawing.Size(105, 21);
            this.cbbQuyen.TabIndex = 2;
            // 
            // bntXacNhan
            // 
            this.bntXacNhan.Location = new System.Drawing.Point(49, 217);
            this.bntXacNhan.Name = "bntXacNhan";
            this.bntXacNhan.Size = new System.Drawing.Size(75, 23);
            this.bntXacNhan.TabIndex = 3;
            this.bntXacNhan.Text = "Xác nhận";
            this.bntXacNhan.UseVisualStyleBackColor = true;
            this.bntXacNhan.Click += new System.EventHandler(this.bntXacNhan_Click);
            // 
            // bntHuyBo
            // 
            this.bntHuyBo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bntHuyBo.Location = new System.Drawing.Point(158, 217);
            this.bntHuyBo.Name = "bntHuyBo";
            this.bntHuyBo.Size = new System.Drawing.Size(75, 23);
            this.bntHuyBo.TabIndex = 4;
            this.bntHuyBo.Text = "Hủy bỏ";
            this.bntHuyBo.UseVisualStyleBackColor = true;
            this.bntHuyBo.Click += new System.EventHandler(this.bntHuyBo_Click);
            // 
            // frmTaiKhoan
            // 
            this.AcceptButton = this.bntXacNhan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bntHuyBo;
            this.ClientSize = new System.Drawing.Size(275, 255);
            this.Controls.Add(this.bntHuyBo);
            this.Controls.Add(this.bntXacNhan);
            this.Controls.Add(this.cbbQuyen);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTenTK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbTen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTaiKhoan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaiKhoan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTenTK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMK;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbbQuyen;
        private System.Windows.Forms.Button bntXacNhan;
        private System.Windows.Forms.Button bntHuyBo;
    }
}