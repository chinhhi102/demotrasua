﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLQuanTraSua.GUI
{
    public partial class frmLogin : Form
    {
        DataTable dt = new DataTable();
        string table_1 = "tbl_taikhoan";
        public frmLogin()
        {
            InitializeComponent();
            Loaddata();
        }

        private void Loaddata()
        {
            this.BackgroundImage = Properties.Resources.Login_background;
            this.pictureBox1.Image = Properties.Resources.Login_trasua;
            dt = SqlServerHelper.ExecuteDataTable(String.Format("Select * from {0}", table_1), CommandType.Text);
        }

        private void bntThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntDangnhap_Click(object sender, EventArgs e)
        {
            bool isCorrect = false;
            DataRow isAc = null;
            foreach(DataRow dr in dt.Rows)
            {
                if(txtTenDangNhap.Text == dr[0].ToString() && txtMatKhau.Text == dr[1].ToString())
                {
                    isAc = dr;
                    isCorrect = true;
                    break;
                }
            }
            if (isCorrect)
            {
                MessageBox.Show("Đăng nhập thành công!", "Thông báo");
                frmMain fMain = new frmMain(isAc);
                this.Hide();
                fMain.ShowDialog();
                Loaddata();
                txtTenDangNhap.Text = txtMatKhau.Text = "";
                this.Show();
                txtTenDangNhap.Focus();
            }
            else
            {
                MessageBox.Show("Đăng nhập thất bại!", "Thông báo");
                txtMatKhau.Text = "";
            }
        }
    }
}
