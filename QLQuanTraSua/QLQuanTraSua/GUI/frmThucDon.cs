﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmThucDon : Form
    {
        private int id = 0;
        private int loai;
        private DataRow dr;
        private DataTable dt;
        public frmThucDon()
        {
            loai = 0;
            InitializeComponent();
            LoadForm();
        }
        public frmThucDon(DataRow _dr)
        {
            loai = 1;
            dr = _dr;
            InitializeComponent();
            LoadForm();
        }

        void LoadForm()
        {
            string sql = @"select * from tbl_nhommon";
            dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
            cbbLoai.Tag = dt;
            foreach (DataRow drr in dt.Rows)
            {
                cbbLoai.Items.Add(drr["tenloai"].ToString());
            }
            if (loai == 0)
            {
                lbTen.Text = "Thêm món";
                cbbLoai.SelectedIndex = 0;
            }
            else
            {
                lbTen.Text = "Sửa món";
                txtTenMon.Text = dr["Tên món"].ToString();
                txtDonGia.Text = dr["Đơn giá"].ToString();
                txtDVT.Text = dr["Đơn vị tính"].ToString();
                foreach (DataRow drr in dt.Rows)
                {
                    if (drr["maloai"].ToString() == dr["Mã loại"].ToString())
                        cbbLoai.Text = drr["tenloai"].ToString();
                }
            }
        }

        private void bntHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntXacNhan_Click(object sender, EventArgs e)
        {
            id = -1;
            foreach (DataRow drrr in dt.Rows)
            {
                if (drrr["tenloai"].ToString() == cbbLoai.Text)
                {
                    id = (int)drrr["maloai"];
                    break;
                }
            }
            if (id == -1)
            {
                MessageBox.Show("Loại không tồn tại", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (loai == 0)
            {
                string sql = @"insert into tbl_thucdon(tenmon,maloai,dongia,donvitinh) values(@tenmon,@maloai,@dongia,@donvitinh)";
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@tenmon", SqlDbType.NVarChar, txtTenMon.Text,
                    "@maloai", SqlDbType.Int, id,
                    "@dongia", SqlDbType.Float, Convert.ToDouble(txtDonGia.Text),
                    "@donvitinh", SqlDbType.NVarChar, txtDVT.Text);
                MessageBox.Show("Thêm thành công", "Thông báo", MessageBoxButtons.OK);
                this.Close();

            }
            else
            {
                string sql = @"update tbl_thucdon set tenmon=@tenmon, maloai=@maloai, dongia=@dongia, donvitinh=@donvitinh where mamon=@vitri";
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@tenmon", SqlDbType.NVarChar, txtTenMon.Text,
                        "@maloai", SqlDbType.Int, id,
                        "@dongia", SqlDbType.Float, Convert.ToDouble(txtDonGia.Text),
                        "@donvitinh", SqlDbType.NVarChar, txtDVT.Text,
                        "@vitri", SqlDbType.NVarChar, dr["Mã món"].ToString());
                MessageBox.Show("Thay đổi thành công!", "Thông báo", MessageBoxButtons.OK);
                this.Close();
            }
        }
    }
}
