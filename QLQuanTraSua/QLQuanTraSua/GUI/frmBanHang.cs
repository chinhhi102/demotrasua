﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmBanHang : Form
    {
        #region Init
        DataTable dt;
        DataTable dtl;
        DataTable dtbill;
        DataRow IDTableSelected, IDBillSelected;
        string table_ban = "tbl_ban";
        string table_nhommon = "tbl_nhommon";
        string table_thucdon = "tbl_thucdon";
        string table_hoadon = "tbl_hoadon";
        string table_chitiethoadon = "tbl_chitiethoadon";
        public frmBanHang()
        {
            InitializeComponent();
            Initform();
            LoadTable();
        }
        void Initform()
        {
            flpLoai.Hide();
            pnlDetail.Hide();
            pnlChonmon.Hide();
            pnlThanhToan.Hide();
        }

        public static int tblWidth = 60, tblHeight = 60, tblHeight_cate = 30, tblWidth_cate = 120, tblWidth_mon = 170, tblHeight_mon = 30, tblWidth_chitiet = 280, tblHeight_chitiet = 40;
        public static Color[] stt = new Color[] { Color.Green, Color.Yellow, Color.DeepPink };
        #endregion

        #region Method
        void refesh()
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (IDTableSelected["maban"].ToString() == dr["maban"].ToString())
                    IDTableSelected = dr;
            }
            DataTable dtbill = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where maban = @maban and trangthai = 0", table_hoadon), CommandType.Text, "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString()));
            if (dtbill.Rows.Count > 0)
            {
                IDBillSelected = dtbill.Rows[0];
            }
        }
        void LoadTable()
        {
            flbTable.Controls.Clear();
            List<Table> tableList = new List<Table>();
            dt = SqlServerHelper.ExecuteDataTable(String.Format("select * from {0}",table_ban), CommandType.Text);
            int SLBanDatTruoc = 0, SLBanDangPV = 0;
            foreach (DataRow dr in dt.Rows)
            {
                Table table = new Table(dr);
                Button bnt = new Button() { Name = dr["maban"].ToString(), Width = tblWidth, Height = tblHeight, Text = dr["tenban"].ToString(), BackColor = stt[(int)dr["trangthai"]] };
                flbTable.Controls.Add(bnt);
                bnt.Click += bntTable_Click;
                bnt.Tag = dr;
                SLBanDatTruoc += ((int)dr["trangthai"] == 1 ? 1 : 0);
                SLBanDangPV += ((int)dr["trangthai"] == 2 ? 1 : 0);
            }
            lbBanDatTruoc.Text = SLBanDatTruoc.ToString();
            lbBanDangPV.Text = SLBanDangPV.ToString();
        }
        void LoadLoai()
        {
            flpLoai.Show();
            flpLoai.Controls.Clear();
            List<Cate> cateList = new List<Cate>();
            dtl = SqlServerHelper.ExecuteDataTable(String.Format("select * from {0}", table_nhommon), CommandType.Text);
            Button f1 = new Button() { Name = "true", Width = tblWidth_cate, Height = tblHeight_cate, BackColor = Color.OrangeRed, Text = "Tất cả" };
            flpLoai.Controls.Add(f1);
            f1.Tag = null;
            f1.Click += bntloai_Click;

            foreach (DataRow dr in dtl.Rows)
            {
                Cate table = new Cate(dr);
                Button bnt = new Button() { Name = dr["maloai"].ToString(), Width = tblWidth_cate, Height = tblHeight_cate, Text = dr["tenloai"].ToString(), BackColor = Color.Orange };
                flpLoai.Controls.Add(bnt);
                bnt.Click += bntloai_Click;
                bnt.Tag = dr;
            }
        }
        void LoadThucDon(string choose)
        {
            pnlChonmon.Show();
            flpThucDon.Controls.Clear();
            List<ThucDon> cateList = new List<ThucDon>();
            if (choose != "*")
            {
                dtl = SqlServerHelper.ExecuteDataTable(String.Format("select * from {0} where maloai = @maloai", table_thucdon), CommandType.Text, "@maloai", SqlDbType.Int, Convert.ToInt32(choose));
            }
            else
            {
                dtl = SqlServerHelper.ExecuteDataTable(String.Format("select * from {0}", table_thucdon), CommandType.Text);
            }
            foreach (DataRow dr in dtl.Rows)
            {
                ThucDon table = new ThucDon(dr);
                Button bnt = new Button() { Name = dr["mamon"].ToString(), Width = tblWidth_mon, Height = tblHeight_mon, Text = dr["tenmon"].ToString(), BackColor = Color.LightYellow };
                flpThucDon.Controls.Add(bnt);
                bnt.Click += bntThucDon_Click;
                bnt.Tag = dr;
            }
        }
        void LoadThanhToan(DataRow drTable)
        {
            pnlThanhToan.Show();
            DataTable dtbill = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where maban = @maban and trangthai = 0", table_hoadon), CommandType.Text, "@maban", SqlDbType.Int, Convert.ToInt32(drTable["maban"].ToString()));
            if (dtbill.Rows.Count > 0)
            {
                IDBillSelected = dtbill.Rows[0];
                flpChitiet.Controls.Clear();
                int tongTien = 0;
                List<Chitiet> chitietList = new List<Chitiet>();
                DataTable dtChitiet = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where mahoadon = @mahoadon", table_chitiethoadon), CommandType.Text, "@mahoadon", SqlDbType.Int, Convert.ToInt32(dtbill.Rows[0]["mahoadon"].ToString()));
                if (dtChitiet.Rows.Count > 0)
                {
                    bntThanhtoan.Text = "Thanh toán";
                    bntThanhtoan.Name = "bntThanhToan";
                    foreach (DataRow dr in dtChitiet.Rows)
                    {
                        DataTable dtMon = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where mamon = @mamon", table_thucdon), CommandType.Text, "@mamon", SqlDbType.Int, dr["mamon"].ToString());
                        Chitiet table = new Chitiet(dr);
                        Button bnt = new Button() { Name = dr["machitiethoadon"].ToString(), Width = tblWidth_chitiet, Height = tblHeight_chitiet, BackColor = Color.Transparent };
                        Label lbTM = new Label() { Text = dtMon.Rows[0]["tenmon"].ToString().Trim(), Location = new Point(5, 12), BackColor = Color.Transparent, Font = new Font("Times New Roman", 12.0f), ForeColor = Color.DarkBlue };
                        Label lbDG = new Label() { Text = dtMon.Rows[0]["dongia"].ToString().Trim() + " VNĐ", Location = new Point(150, 12), Width = 60, BackColor = Color.Transparent, Font = new Font("Times New Roman", 12.0f), ForeColor = Color.Red };
                        Label lbSL = new Label() { Text = dr["soluong"].ToString().Trim() + " " + dtMon.Rows[0]["donvitinh"].ToString().Trim(), Width = 30, Location = new Point(210, 12), BackColor = Color.Transparent, Font = new Font("Times New Roman", 12.0f), ForeColor = Color.Green };
                        Button bntX = new Button() { Text = "X", Width = 20, Location = new Point(240, 10) };
                        bntX.Tag = dr;
                        bntX.Click += bntX_Click;
                        bnt.Controls.Add(lbTM);
                        bnt.Controls.Add(lbDG);
                        bnt.Controls.Add(lbSL);
                        bnt.Controls.Add(bntX);
                        flpChitiet.Controls.Add(bnt);
                        bnt.Click += bntChitiet_Click;
                        bnt.Tag = dr;
                        tongTien += Convert.ToInt32(dtMon.Rows[0]["dongia"].ToString()) * Convert.ToInt32(dr["soluong"]);
                    }
                }
                else
                {
                    bntThanhtoan.Text = "Huỷ đặt";
                    bntThanhtoan.Name = "bntHuyGoiMon";
                }
                lbTienMon.Text = tongTien.ToString();
                cbbGiamgia.Text = dtbill.Rows[0]["giamgia"].ToString();
                lbThanhtien.Text = (tongTien - (((cbbGiamgia.Text == "") ? 0 : Convert.ToInt32(cbbGiamgia.Text) / 100.0) * tongTien)).ToString();
            }
            else
            {
                pnlThanhToan.Hide();
            }
            
        }
        void showBill(DataRow dr)
        {
            lbTenBan.Text = dr["tenban"].ToString();
            lbTenBan.Name = dr["maban"].ToString();
            int tt = (int)dr["trangthai"];
            lbTrangThai.Text = (tt == 0 ? "Còn trống" : tt == 1 ? "Đã đặt trước" : "Đang phục vụ");
            bntDatCho.Text = (tt == 0 ? "Đặt chỗ" : "Hủy đặt");
            bntDatCho.Name = (tt == 0 ? "bntDatCho" : "bntHuyDat");
            pnlDetail.Show();
            if (tt == 2)
            {
                LoadLoai();
                LoadThucDon("*");
                LoadThanhToan(dr);
                lbGioDen.Text = IDBillSelected["gioden"].ToString();
            }
            else
            {
                flpLoai.Hide();
                pnlChonmon.Hide();
                pnlThanhToan.Hide();
                lbGioDen.Text = "...";
            }
        }
        void showBillInfo(string id)
        {
            flpChitiet.Controls.Clear();
            if (id == "-1")
            {
                bntThanhtoan.Text = "Hủy đặt";
                bntThanhtoan.Name = "bntHuyGoiMon";
            }
            else
            {
                int tongTien = 0;
                List<Chitiet> chitietList = new List<Chitiet>();
                DataTable dtChitiet = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where mahoadon = @mahoadon", table_chitiethoadon), CommandType.Text, "@mahoadon", SqlDbType.Int, Convert.ToInt32(id));
                if (dtChitiet.Rows.Count > 0)
                {
                    bntThanhtoan.Text = "Thanh toán";
                    bntThanhtoan.Name = "bntThanhToan";
                    
                    foreach (DataRow dr in dtChitiet.Rows)
                    {
                        DataTable dtMon = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where mamon = @mamon", table_thucdon), CommandType.Text, "@mamon", SqlDbType.Int, dr["mamon"].ToString());
                        Chitiet table = new Chitiet(dr);
                        Button bnt = new Button() { Name = dr["machitiethoadon"].ToString(), Width = tblWidth_chitiet, Height = tblHeight_chitiet, BackColor = Color.Transparent };
                        Label lbTM = new Label() { Text = dtMon.Rows[0]["tenmon"].ToString().Trim(), Location = new Point(5, 12), BackColor = Color.Transparent, Font = new Font("Times New Roman", 12.0f), ForeColor = Color.DarkBlue };
                        Label lbDG = new Label() { Text = dtMon.Rows[0]["dongia"].ToString().Trim() + " VNĐ", Location = new Point(150, 12), Width = 60, BackColor = Color.Transparent, Font = new Font("Times New Roman", 12.0f), ForeColor = Color.Red };
                        Label lbSL = new Label() { Text = dr["soluong"].ToString().Trim() + " " + dtMon.Rows[0]["donvitinh"].ToString().Trim(), Width = 30, Location = new Point(210, 12), BackColor = Color.Transparent, Font = new Font("Times New Roman", 12.0f), ForeColor = Color.Green };
                        Button bntX = new Button() { Text = "X", Width = 20, Location = new Point(240, 10) };
                        bntX.Tag = dr;
                        bntX.Click += bntX_Click;
                        bnt.Controls.Add(lbTM);
                        bnt.Controls.Add(lbDG);
                        bnt.Controls.Add(lbSL);
                        bnt.Controls.Add(bntX);
                        flpChitiet.Controls.Add(bnt);
                        bnt.Click += bntChitiet_Click;
                        bnt.Tag = dr;
                        tongTien += Convert.ToInt32(dtMon.Rows[0]["dongia"].ToString()) * Convert.ToInt32(dr["soluong"]);
                    }
                    
                }
                else
                {
                    bntThanhtoan.Text = "Huỷ đặt";
                    bntThanhtoan.Name = "bntHuyGoiMon";
                }
                lbTienMon.Text = tongTien.ToString();
                lbThanhtien.Text = (tongTien - (((cbbGiamgia.Text == "") ? 0 : Convert.ToInt32(cbbGiamgia.Text) / 100.0) * tongTien)).ToString();
                string sql = string.Format(@"update {0} set tongtien = @tongtien, giamgia = @giamgia where maban = @maban", table_hoadon);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"]),
                    "@tongtien", SqlDbType.Float, Convert.ToDouble(lbThanhtien.Text),
                    "@giamgia", SqlDbType.Float, Convert.ToDouble(cbbGiamgia.Text)
                    );
            }
        }


        #endregion

        #region Event
        private void bntThanhtoan_Click(object sender, EventArgs e)
        {
            if (bntThanhtoan.Name == "bntHuyGoiMon")
            {
                string sql = string.Format(@"update {0} set trangthai = 0 where maban = @maban", table_ban);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString())
                    );
                LoadTable();
                sql = string.Format("delete from {1} where mahoadon = @mahoadon", table_chitiethoadon, table_hoadon);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@mahoadon", SqlDbType.Int, Convert.ToInt32(IDBillSelected["mahoadon"].ToString())
                    );
                refesh();
                showBill(IDTableSelected);
            }
            else
            {
                refesh();
                frmThanhToan fThanhToan = new frmThanhToan(lbTenBan.Text, IDBillSelected["mahoadon"].ToString(), lbTienMon.Text, IDBillSelected);
                if (fThanhToan.ShowDialog() == DialogResult.Yes)
                {
                    string sql = string.Format(@"update {0} set trangthai = 0 where maban = @maban", table_ban);
                    SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString())
                        );
                    LoadTable();
                    refesh();
                    showBill(IDTableSelected);
                }
            }
        }
        private void cbbGiamgia_TextChanged(object sender, EventArgs e)
        {
            int tongTien = Convert.ToInt32(lbTienMon.Text);
            lbThanhtien.Text = (tongTien - (((cbbGiamgia.Text == "") ? 0 : Convert.ToInt32(cbbGiamgia.Text)/100.0) * tongTien)).ToString();
            string sql = string.Format(@"update {0} set tongtien = @tongtien, giamgia = @giamgia where maban = @maban", table_hoadon);
            SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                "@maban",SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"]),
                "@tongtien", SqlDbType.Float, Convert.ToDouble(lbThanhtien.Text),
                "@giamgia",SqlDbType.Float, Convert.ToDouble(cbbGiamgia.Text)
                );
        }

        private void flpChitiet_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmBanHang_Activated(object sender, EventArgs e)
        {
            Initform();
            LoadTable();
        }

        private void bntTroVe1_Click(object sender, EventArgs e)
        {
            pnlDetail.Hide();
        }
        private void bntDatCho_Click(object sender, EventArgs e)
        {
            if (bntDatCho.Name == "bntDatCho")
            {
                string sql = string.Format(@"update {0} set trangthai = 1 where maban = @maban", table_ban);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString())
                    );
                LoadTable();
                bntDatCho.Name = "bntHuyDat";
                bntDatCho.Text = "Hủy đặt";
                lbTrangThai.Text = "Đã đặt trước";
            }
            else
            {
                string sql = string.Format(@"update {0} set trangthai = 0 where maban = @maban", table_ban);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString())
                    );
                LoadTable();
                bntDatCho.Name = "bntDatCho";
                bntDatCho.Text = "Đặt chỗ";
                lbTrangThai.Text = "Còn trống";
            }
        }
        private void bntGoiMon_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            string sql = string.Format(@"update {0} set trangthai = 2 where maban = @maban", table_ban);
            SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString())
                );
            LoadTable();
            sql = string.Format(@"insert into {0}(giamgia,maban,gioden,tongtien,trangthai)
                                         values (@giamgia,@maban,@gioden,@tongtien,@trangthai)", table_hoadon);
            SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                "@giamgia", SqlDbType.Float, Convert.ToDouble(0),
                "@maban", SqlDbType.Int, Convert.ToInt32(IDTableSelected["maban"].ToString()),
                "@gioden", SqlDbType.DateTime, now,
                "@tongtien", SqlDbType.Float, Convert.ToDouble(0),
                "@trangthai", SqlDbType.Int, Convert.ToInt32(0)
                );
            refesh();
            showBill(IDTableSelected);
        }
        void bntThucDon_Click(object sender, EventArgs e)
        {
            DataRow dr = ((sender as Button).Tag as DataRow);
            frmChonThucDon frmForm = new frmChonThucDon(IDTableSelected["tenban"].ToString(), IDBillSelected["mahoadon"].ToString(), dr);
            frmForm.ShowDialog();
            string maban = IDTableSelected["maban"].ToString();
            dtbill = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where maban = @maban and trangthai = 0", table_hoadon), CommandType.Text, "@maban", SqlDbType.Int, maban);
            showBillInfo(dtbill.Rows[0]["mahoadon"].ToString());
        }
        void bntloai_Click(object sender, EventArgs e)
        {
            if((sender as Button).Tag == null)
            {
                LoadThucDon("*");
            }
            else
            {
                LoadThucDon(((sender as Button).Tag as DataRow)["maloai"].ToString());
            }
        }
        void bntChitiet_Click(object sender, EventArgs e)
        {
             
        }
        void bntTable_Click(object sender, EventArgs e)
        {
            DataRow dr = ((sender as Button).Tag as DataRow);
            IDTableSelected = dr;
            showBill(((sender as Button).Tag) as DataRow);
        }
        void bntX_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Bạn có chắc chắn xóa không?","Cảnh báo",MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DataRow dr = ((sender as Button).Tag as DataRow);
                string sql = string.Format("delete from {0} where machitiethoadon = @machitiethoadon", table_chitiethoadon);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@machitiethoadon", SqlDbType.Int, Convert.ToInt32(dr["machitiethoadon"].ToString())
                    );
                showBillInfo(IDBillSelected["mahoadon"].ToString());
            }           
        }
        #endregion
    }
}
