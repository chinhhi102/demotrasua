﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        #region Init

        DataRow user;
        private frmQuanLy fQuanLy = null;
        private frmBanHang fBanHang = null;
        private frmThongke fThongKe = null;
        private frmCaidat fCaidat = null;
        public frmMain(DataRow _user)
        {
            user = _user;
            InitializeComponent();
            InitForm();
            lb_TaiKhoan.Text = user["tentaikhoan"].ToString();
        }
        private void InitForm()
        {
            this.BackgroundImage = Properties.Resources.Main_BG;
            if(user["quyen"].ToString() == "1")
            {
                bntQuanLy.Enabled = false;
                bntThongKe.Enabled = false;
            }
        }

        #endregion

        #region Method
        private Form ShowOrActiveForm(Form form, Type t)
        {
            if (form == null)
            {
                form = (Form)Activator.CreateInstance(t);
                form.MdiParent = this;
                form.Show();
                form.Location = new Point(140, 70);
            }
            else
            {
                if (form.IsDisposed)
                {
                    form = (Form)Activator.CreateInstance(t);
                    form.MdiParent = this;
                    form.Show();
                    form.Location = new Point(140, 70);
                }
                else
                {
                    form.Show();
                    form.Activate();
                    form.Location = new Point(140, 70);
                }
            }
            return form;
        }
        #endregion

        #region Event
        private void bntTrangChu_Click(object sender, EventArgs e)
        {
            foreach (Form frm in this.MdiChildren)
            {
                frm.Hide();
            }
        }

        private void bntQuanLy_Click(object sender, EventArgs e)
        {
            fQuanLy = ShowOrActiveForm(fQuanLy, typeof(frmQuanLy)) as frmQuanLy;
        }

        private void bntBanHang_Click(object sender, EventArgs e)
        {
            fBanHang = ShowOrActiveForm(fBanHang, typeof(frmBanHang)) as frmBanHang;
        }

        private void bntThietLap_Click(object sender, EventArgs e)
        {
            fCaidat = ShowOrActiveForm(fCaidat, typeof(frmCaidat)) as frmCaidat;
        }

        private void bntThongKe_Click(object sender, EventArgs e)
        {
            fThongKe = ShowOrActiveForm(fThongKe, typeof(frmThongke)) as frmThongke;
        }

        private void bntTrangChu_MouseHover(object sender, EventArgs e)
        {
            ((Button)sender).Size = new System.Drawing.Size(125, 50);
        }

        private void bntTrangChu_MouseLeave(object sender, EventArgs e)
        {
            ((Button)sender).Size = new System.Drawing.Size(118, 44);
        }

        private void bntDangXuat_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn đăng xuất không?", "Thông báo!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }
        #endregion

    }
}
