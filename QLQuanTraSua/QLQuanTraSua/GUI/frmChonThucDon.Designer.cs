﻿namespace QLQuanTraSua.GUI
{
    partial class frmChonThucDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbTenBan = new System.Windows.Forms.Label();
            this.lbTenMon = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbDvt = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGia = new System.Windows.Forms.TextBox();
            this.bntDongy = new System.Windows.Forms.Button();
            this.bntHuy = new System.Windows.Forms.Button();
            this.bntAdd = new System.Windows.Forms.Button();
            this.bntSub = new System.Windows.Forms.Button();
            this.txtSL = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbTenBan
            // 
            this.lbTenBan.AutoSize = true;
            this.lbTenBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenBan.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lbTenBan.Location = new System.Drawing.Point(12, 18);
            this.lbTenBan.Name = "lbTenBan";
            this.lbTenBan.Size = new System.Drawing.Size(58, 24);
            this.lbTenBan.TabIndex = 0;
            this.lbTenBan.Text = "none";
            // 
            // lbTenMon
            // 
            this.lbTenMon.AutoSize = true;
            this.lbTenMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenMon.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lbTenMon.Location = new System.Drawing.Point(96, 29);
            this.lbTenMon.Name = "lbTenMon";
            this.lbTenMon.Size = new System.Drawing.Size(54, 24);
            this.lbTenMon.TabIndex = 0;
            this.lbTenMon.Text = "none";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số lượng";
            // 
            // lbDvt
            // 
            this.lbDvt.AutoSize = true;
            this.lbDvt.Location = new System.Drawing.Point(164, 68);
            this.lbDvt.Name = "lbDvt";
            this.lbDvt.Size = new System.Drawing.Size(33, 13);
            this.lbDvt.TabIndex = 0;
            this.lbDvt.Text = "None";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Giá:";
            // 
            // txtGia
            // 
            this.txtGia.Location = new System.Drawing.Point(129, 120);
            this.txtGia.Name = "txtGia";
            this.txtGia.Size = new System.Drawing.Size(100, 20);
            this.txtGia.TabIndex = 2;
            // 
            // bntDongy
            // 
            this.bntDongy.Location = new System.Drawing.Point(71, 156);
            this.bntDongy.Name = "bntDongy";
            this.bntDongy.Size = new System.Drawing.Size(75, 23);
            this.bntDongy.TabIndex = 3;
            this.bntDongy.Text = "Đồng ý";
            this.bntDongy.UseVisualStyleBackColor = true;
            this.bntDongy.Click += new System.EventHandler(this.bntDongy_Click);
            // 
            // bntHuy
            // 
            this.bntHuy.Location = new System.Drawing.Point(152, 156);
            this.bntHuy.Name = "bntHuy";
            this.bntHuy.Size = new System.Drawing.Size(75, 23);
            this.bntHuy.TabIndex = 3;
            this.bntHuy.Text = "Hủy";
            this.bntHuy.UseVisualStyleBackColor = true;
            this.bntHuy.Click += new System.EventHandler(this.bntHuy_Click);
            // 
            // bntAdd
            // 
            this.bntAdd.BackgroundImage = global::QLQuanTraSua.Properties.Resources.add;
            this.bntAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntAdd.Location = new System.Drawing.Point(240, 63);
            this.bntAdd.Name = "bntAdd";
            this.bntAdd.Size = new System.Drawing.Size(31, 23);
            this.bntAdd.TabIndex = 1;
            this.bntAdd.UseVisualStyleBackColor = true;
            this.bntAdd.Click += new System.EventHandler(this.bntAdd_Click);
            // 
            // bntSub
            // 
            this.bntSub.BackgroundImage = global::QLQuanTraSua.Properties.Resources.sub;
            this.bntSub.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bntSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntSub.Location = new System.Drawing.Point(203, 63);
            this.bntSub.Name = "bntSub";
            this.bntSub.Size = new System.Drawing.Size(31, 23);
            this.bntSub.TabIndex = 1;
            this.bntSub.UseVisualStyleBackColor = true;
            this.bntSub.Click += new System.EventHandler(this.bntSub_Click);
            // 
            // txtSL
            // 
            this.txtSL.Location = new System.Drawing.Point(96, 65);
            this.txtSL.Name = "txtSL";
            this.txtSL.Size = new System.Drawing.Size(47, 20);
            this.txtSL.TabIndex = 2;
            this.txtSL.TextChanged += new System.EventHandler(this.txtSL_TextChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmChonThucDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(300, 200);
            this.Controls.Add(this.bntHuy);
            this.Controls.Add(this.bntDongy);
            this.Controls.Add(this.txtSL);
            this.Controls.Add(this.txtGia);
            this.Controls.Add(this.bntAdd);
            this.Controls.Add(this.bntSub);
            this.Controls.Add(this.lbDvt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbTenMon);
            this.Controls.Add(this.lbTenBan);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmChonThucDon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmChonThucDon";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTenBan;
        private System.Windows.Forms.Label lbTenMon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbDvt;
        private System.Windows.Forms.Button bntSub;
        private System.Windows.Forms.Button bntAdd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGia;
        private System.Windows.Forms.Button bntDongy;
        private System.Windows.Forms.Button bntHuy;
        private System.Windows.Forms.TextBox txtSL;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}