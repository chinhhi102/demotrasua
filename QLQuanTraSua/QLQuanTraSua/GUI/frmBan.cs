﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmBan : Form
    {
        private int loai;
        private DataRow dr;

        public frmBan()
        {
            loai = 0;
            InitializeComponent();
            LoadForm();
        }

        public frmBan(DataRow _dr)
        {
            loai = 1;
            dr = _dr;
            InitializeComponent();
            LoadForm();
        }
        void LoadForm()
        {
            if (loai == 0)
            {
                lbTen.Text = "Thêm Bàn";
            }
            else
            {
                lbTen.Text = "Sửa Bàn";
                txtTenban.Text = dr["Tên bàn"].ToString().Substring(4);
            }
        }
        private void bntHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntXacNhan_Click(object sender, EventArgs e)
        {
            if (loai == 0)
            {
                string sql = @"select * from tbl_ban where tenban=@tenban";
                DataTable dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text, "@tenban", SqlDbType.NVarChar, txtTenban.Text);
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Bàn đã tồn tại", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    sql = @"insert into tbl_ban(tenban) values(N'Bàn ' + @tenban)";
                    SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@tenban", SqlDbType.NVarChar, txtTenban.Text);
                    MessageBox.Show("Thêm thành công", "Thông báo", MessageBoxButtons.OK);
                    this.Close();
                }
            }
            else
            {
                string sql = @"update tbl_ban set tenban=N'Bàn ' + @tenban where tenban=@vitri";
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@tenban", SqlDbType.NVarChar, txtTenban.Text,
                        "@vitri", SqlDbType.NVarChar, dr["Tên bàn"].ToString());
                MessageBox.Show("Thay đổi thành công!", "Thông báo", MessageBoxButtons.OK);
                this.Close();
            }
        }
    }
}
