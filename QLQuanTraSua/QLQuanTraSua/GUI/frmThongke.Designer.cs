﻿namespace QLQuanTraSua.GUI
{
    partial class frmThongke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtgvTKTheoHoaDon = new System.Windows.Forms.DataGridView();
            this.dtgvTKMon = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.lbTongSoHoaDon = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbTienMon = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbTienGiamGia = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbTongSoMonDaBan = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbTienThuVe = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbTongSoBan = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbTongSoMon = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lbTongSoLoai = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lbTongSoTaiKhoan = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.bntThongKe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTKTheoHoaDon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTKMon)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(316, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Từ ngày:";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(378, 33);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(197, 20);
            this.dtpTo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(737, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Đến ngày:";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(799, 32);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(197, 20);
            this.dtpFrom.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(38, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(199, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Thống kê theo hóa đơn";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(819, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 22);
            this.label4.TabIndex = 2;
            this.label4.Text = "Thống kê theo món";
            // 
            // dtgvTKTheoHoaDon
            // 
            this.dtgvTKTheoHoaDon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvTKTheoHoaDon.Location = new System.Drawing.Point(12, 111);
            this.dtgvTKTheoHoaDon.Name = "dtgvTKTheoHoaDon";
            this.dtgvTKTheoHoaDon.ReadOnly = true;
            this.dtgvTKTheoHoaDon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvTKTheoHoaDon.Size = new System.Drawing.Size(531, 262);
            this.dtgvTKTheoHoaDon.TabIndex = 3;
            // 
            // dtgvTKMon
            // 
            this.dtgvTKMon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvTKMon.Location = new System.Drawing.Point(549, 111);
            this.dtgvTKMon.Name = "dtgvTKMon";
            this.dtgvTKMon.ReadOnly = true;
            this.dtgvTKMon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvTKMon.Size = new System.Drawing.Size(469, 262);
            this.dtgvTKMon.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(12, 389);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Tổng số hóa đơn thanh toán: ";
            // 
            // lbTongSoHoaDon
            // 
            this.lbTongSoHoaDon.AutoSize = true;
            this.lbTongSoHoaDon.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSoHoaDon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTongSoHoaDon.Location = new System.Drawing.Point(180, 389);
            this.lbTongSoHoaDon.Name = "lbTongSoHoaDon";
            this.lbTongSoHoaDon.Size = new System.Drawing.Size(14, 15);
            this.lbTongSoHoaDon.TabIndex = 2;
            this.lbTongSoHoaDon.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(109, 413);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "Tiền món:";
            // 
            // lbTienMon
            // 
            this.lbTienMon.AutoSize = true;
            this.lbTienMon.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienMon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTienMon.Location = new System.Drawing.Point(180, 413);
            this.lbTienMon.Name = "lbTienMon";
            this.lbTienMon.Size = new System.Drawing.Size(14, 15);
            this.lbTienMon.TabIndex = 2;
            this.lbTienMon.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(85, 438);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 15);
            this.label9.TabIndex = 2;
            this.label9.Text = "Tiền giảm giá:";
            // 
            // lbTienGiamGia
            // 
            this.lbTienGiamGia.AutoSize = true;
            this.lbTienGiamGia.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienGiamGia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTienGiamGia.Location = new System.Drawing.Point(180, 438);
            this.lbTienGiamGia.Name = "lbTienGiamGia";
            this.lbTienGiamGia.Size = new System.Drawing.Size(14, 15);
            this.lbTienGiamGia.TabIndex = 2;
            this.lbTienGiamGia.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(546, 389);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "Tổng số món đã bán:";
            // 
            // lbTongSoMonDaBan
            // 
            this.lbTongSoMonDaBan.AutoSize = true;
            this.lbTongSoMonDaBan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSoMonDaBan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTongSoMonDaBan.Location = new System.Drawing.Point(669, 389);
            this.lbTongSoMonDaBan.Name = "lbTongSoMonDaBan";
            this.lbTongSoMonDaBan.Size = new System.Drawing.Size(14, 15);
            this.lbTongSoMonDaBan.TabIndex = 2;
            this.lbTongSoMonDaBan.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label13.Location = new System.Drawing.Point(100, 476);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 15);
            this.label13.TabIndex = 2;
            this.label13.Text = "Tiền thu về:";
            // 
            // lbTienThuVe
            // 
            this.lbTienThuVe.AutoSize = true;
            this.lbTienThuVe.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienThuVe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTienThuVe.Location = new System.Drawing.Point(180, 476);
            this.lbTienThuVe.Name = "lbTienThuVe";
            this.lbTienThuVe.Size = new System.Drawing.Size(14, 15);
            this.lbTienThuVe.TabIndex = 2;
            this.lbTienThuVe.Text = "1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label15.Location = new System.Drawing.Point(353, 476);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 15);
            this.label15.TabIndex = 2;
            this.label15.Text = "Tổng số bàn:";
            // 
            // lbTongSoBan
            // 
            this.lbTongSoBan.AutoSize = true;
            this.lbTongSoBan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSoBan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTongSoBan.Location = new System.Drawing.Point(435, 476);
            this.lbTongSoBan.Name = "lbTongSoBan";
            this.lbTongSoBan.Size = new System.Drawing.Size(14, 15);
            this.lbTongSoBan.TabIndex = 2;
            this.lbTongSoBan.Text = "1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(470, 476);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 15);
            this.label17.TabIndex = 2;
            this.label17.Text = "Tổng số món:";
            // 
            // lbTongSoMon
            // 
            this.lbTongSoMon.AutoSize = true;
            this.lbTongSoMon.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSoMon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTongSoMon.Location = new System.Drawing.Point(552, 476);
            this.lbTongSoMon.Name = "lbTongSoMon";
            this.lbTongSoMon.Size = new System.Drawing.Size(14, 15);
            this.lbTongSoMon.TabIndex = 2;
            this.lbTongSoMon.Text = "1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label19.Location = new System.Drawing.Point(592, 476);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 15);
            this.label19.TabIndex = 2;
            this.label19.Text = "Tống số loại:";
            // 
            // lbTongSoLoai
            // 
            this.lbTongSoLoai.AutoSize = true;
            this.lbTongSoLoai.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSoLoai.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTongSoLoai.Location = new System.Drawing.Point(675, 476);
            this.lbTongSoLoai.Name = "lbTongSoLoai";
            this.lbTongSoLoai.Size = new System.Drawing.Size(14, 15);
            this.lbTongSoLoai.TabIndex = 2;
            this.lbTongSoLoai.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(714, 476);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(104, 15);
            this.label21.TabIndex = 2;
            this.label21.Text = "Tổng số tài khoản";
            // 
            // lbTongSoTaiKhoan
            // 
            this.lbTongSoTaiKhoan.AutoSize = true;
            this.lbTongSoTaiKhoan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTongSoTaiKhoan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lbTongSoTaiKhoan.Location = new System.Drawing.Point(824, 476);
            this.lbTongSoTaiKhoan.Name = "lbTongSoTaiKhoan";
            this.lbTongSoTaiKhoan.Size = new System.Drawing.Size(14, 15);
            this.lbTongSoTaiKhoan.TabIndex = 2;
            this.lbTongSoTaiKhoan.Text = "1";
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(39, 453);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(190, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = ".................................................";
            // 
            // bntThongKe
            // 
            this.bntThongKe.Location = new System.Drawing.Point(595, 32);
            this.bntThongKe.Name = "bntThongKe";
            this.bntThongKe.Size = new System.Drawing.Size(136, 23);
            this.bntThongKe.TabIndex = 5;
            this.bntThongKe.Text = "Thống kê";
            this.bntThongKe.UseVisualStyleBackColor = true;
            this.bntThongKe.Click += new System.EventHandler(this.bntThongKe_Click);
            // 
            // frmThongke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 500);
            this.Controls.Add(this.bntThongKe);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.dtgvTKMon);
            this.Controls.Add(this.dtgvTKTheoHoaDon);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbTongSoTaiKhoan);
            this.Controls.Add(this.lbTongSoLoai);
            this.Controls.Add(this.lbTongSoMon);
            this.Controls.Add(this.lbTongSoBan);
            this.Controls.Add(this.lbTienThuVe);
            this.Controls.Add(this.lbTienGiamGia);
            this.Controls.Add(this.lbTienMon);
            this.Controls.Add(this.lbTongSoMonDaBan);
            this.Controls.Add(this.lbTongSoHoaDon);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(140, 70);
            this.Name = "frmThongke";
            this.Text = "Thongke";
            this.Activated += new System.EventHandler(this.frmThongke_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTKTheoHoaDon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTKMon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dtgvTKTheoHoaDon;
        private System.Windows.Forms.DataGridView dtgvTKMon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbTongSoHoaDon;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbTienMon;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbTienGiamGia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbTongSoMonDaBan;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbTienThuVe;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbTongSoBan;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbTongSoMon;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbTongSoLoai;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lbTongSoTaiKhoan;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button bntThongKe;
    }
}