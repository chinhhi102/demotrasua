﻿namespace QLQuanTraSua.GUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.bntBanHang = new System.Windows.Forms.Button();
            this.bntQuanLy = new System.Windows.Forms.Button();
            this.bntThongKe = new System.Windows.Forms.Button();
            this.bntThietLap = new System.Windows.Forms.Button();
            this.bntTrangChu = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bntDangXuat = new System.Windows.Forms.Button();
            this.lb_TaiKhoan = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bntBanHang
            // 
            this.bntBanHang.BackColor = System.Drawing.Color.SeaGreen;
            this.bntBanHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntBanHang.Image = ((System.Drawing.Image)(resources.GetObject("bntBanHang.Image")));
            this.bntBanHang.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bntBanHang.Location = new System.Drawing.Point(12, 100);
            this.bntBanHang.Name = "bntBanHang";
            this.bntBanHang.Size = new System.Drawing.Size(118, 44);
            this.bntBanHang.TabIndex = 1;
            this.bntBanHang.Text = "BÁN HÀNG";
            this.bntBanHang.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bntBanHang.UseVisualStyleBackColor = false;
            this.bntBanHang.Click += new System.EventHandler(this.bntBanHang_Click);
            this.bntBanHang.MouseLeave += new System.EventHandler(this.bntTrangChu_MouseLeave);
            this.bntBanHang.MouseHover += new System.EventHandler(this.bntTrangChu_MouseHover);
            // 
            // bntQuanLy
            // 
            this.bntQuanLy.BackColor = System.Drawing.Color.SeaGreen;
            this.bntQuanLy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntQuanLy.Location = new System.Drawing.Point(12, 160);
            this.bntQuanLy.Name = "bntQuanLy";
            this.bntQuanLy.Size = new System.Drawing.Size(118, 44);
            this.bntQuanLy.TabIndex = 2;
            this.bntQuanLy.Text = "QUẢN LÝ";
            this.bntQuanLy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bntQuanLy.UseVisualStyleBackColor = false;
            this.bntQuanLy.Click += new System.EventHandler(this.bntQuanLy_Click);
            this.bntQuanLy.MouseLeave += new System.EventHandler(this.bntTrangChu_MouseLeave);
            this.bntQuanLy.MouseHover += new System.EventHandler(this.bntTrangChu_MouseHover);
            // 
            // bntThongKe
            // 
            this.bntThongKe.BackColor = System.Drawing.Color.SeaGreen;
            this.bntThongKe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntThongKe.Location = new System.Drawing.Point(12, 220);
            this.bntThongKe.Name = "bntThongKe";
            this.bntThongKe.Size = new System.Drawing.Size(118, 44);
            this.bntThongKe.TabIndex = 3;
            this.bntThongKe.Text = "THỐNG KÊ";
            this.bntThongKe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bntThongKe.UseVisualStyleBackColor = false;
            this.bntThongKe.Click += new System.EventHandler(this.bntThongKe_Click);
            this.bntThongKe.MouseLeave += new System.EventHandler(this.bntTrangChu_MouseLeave);
            this.bntThongKe.MouseHover += new System.EventHandler(this.bntTrangChu_MouseHover);
            // 
            // bntThietLap
            // 
            this.bntThietLap.BackColor = System.Drawing.Color.SeaGreen;
            this.bntThietLap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntThietLap.Location = new System.Drawing.Point(12, 280);
            this.bntThietLap.Name = "bntThietLap";
            this.bntThietLap.Size = new System.Drawing.Size(118, 44);
            this.bntThietLap.TabIndex = 4;
            this.bntThietLap.Text = "THIẾT LẬP";
            this.bntThietLap.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bntThietLap.UseVisualStyleBackColor = false;
            this.bntThietLap.Click += new System.EventHandler(this.bntThietLap_Click);
            this.bntThietLap.MouseLeave += new System.EventHandler(this.bntTrangChu_MouseLeave);
            this.bntThietLap.MouseHover += new System.EventHandler(this.bntTrangChu_MouseHover);
            // 
            // bntTrangChu
            // 
            this.bntTrangChu.BackColor = System.Drawing.Color.SeaGreen;
            this.bntTrangChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntTrangChu.Image = ((System.Drawing.Image)(resources.GetObject("bntTrangChu.Image")));
            this.bntTrangChu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bntTrangChu.Location = new System.Drawing.Point(12, 40);
            this.bntTrangChu.Name = "bntTrangChu";
            this.bntTrangChu.Size = new System.Drawing.Size(118, 44);
            this.bntTrangChu.TabIndex = 0;
            this.bntTrangChu.Text = "TRANG CHỦ";
            this.bntTrangChu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bntTrangChu.UseVisualStyleBackColor = false;
            this.bntTrangChu.Click += new System.EventHandler(this.bntTrangChu_Click);
            this.bntTrangChu.MouseLeave += new System.EventHandler(this.bntTrangChu_MouseLeave);
            this.bntTrangChu.MouseHover += new System.EventHandler(this.bntTrangChu_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bntDangXuat);
            this.groupBox1.Controls.Add(this.lb_TaiKhoan);
            this.groupBox1.Location = new System.Drawing.Point(1080, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(108, 53);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TÀI KHOẢN";
            // 
            // bntDangXuat
            // 
            this.bntDangXuat.Location = new System.Drawing.Point(6, 28);
            this.bntDangXuat.Name = "bntDangXuat";
            this.bntDangXuat.Size = new System.Drawing.Size(96, 23);
            this.bntDangXuat.TabIndex = 1;
            this.bntDangXuat.Text = "Đăng xuất";
            this.bntDangXuat.UseVisualStyleBackColor = true;
            this.bntDangXuat.Click += new System.EventHandler(this.bntDangXuat_Click);
            // 
            // lb_TaiKhoan
            // 
            this.lb_TaiKhoan.AutoSize = true;
            this.lb_TaiKhoan.Location = new System.Drawing.Point(26, 12);
            this.lb_TaiKhoan.Name = "lb_TaiKhoan";
            this.lb_TaiKhoan.Size = new System.Drawing.Size(10, 13);
            this.lb_TaiKhoan.TabIndex = 0;
            this.lb_TaiKhoan.Text = ".";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1200, 600);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bntTrangChu);
            this.Controls.Add(this.bntBanHang);
            this.Controls.Add(this.bntThietLap);
            this.Controls.Add(this.bntThongKe);
            this.Controls.Add(this.bntQuanLy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button bntBanHang;
        private System.Windows.Forms.Button bntQuanLy;
        private System.Windows.Forms.Button bntThongKe;
        private System.Windows.Forms.Button bntThietLap;
        private System.Windows.Forms.Button bntTrangChu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lb_TaiKhoan;
        private System.Windows.Forms.Button bntDangXuat;
    }
}