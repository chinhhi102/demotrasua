﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmThanhToan : Form
    {
        public frmThanhToan()
        {
            InitializeComponent();
        }
        string TenBan, mahoadon;
        DataRow dr;

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = string.Format("Update tbl_hoadon set trangthai = 1 where mahoadon = @mahoadon");
            SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text, "@mahoadon", SqlDbType.Int, mahoadon);
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        public frmThanhToan(string _TenBan, string _mahoadon, string tienmon, DataRow _dr)
        {
            InitializeComponent();
            TenBan = _TenBan;
            mahoadon = _mahoadon;
            dr = _dr;
            lbTenBan.Text = _TenBan;
            lbTM.Text = tienmon + " VNĐ";
            lbGG.Text = dr["giamgia"].ToString() + " %";
            lbTien.Text = dr["tongtien"].ToString() + " VNĐ";
        }
    }
}
