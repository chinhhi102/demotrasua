﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace QLQuanTraSua
{
    public class Table
    {
        public Table(int _maban, string _tenban, int _trangthai)
        {
            this.maban = _maban;
            this.tenban = _tenban;
            this.trangthai = _trangthai;
        }
        public Table(DataRow row)
        {
            this.maban = (int)row["maban"];
            this.tenban = row["tenban"].ToString();
            this.trangthai = (int)row["trangthai"];
        }
        private int maban;
        private string tenban;
        private int trangthai;
        public int Maban
        {
            get { return maban; }
            set { maban = value; }
        }
        public string Tenban
        {
            get { return tenban; }
            set { tenban = value; }
        }
        public int Trangthai
        {
            get { return trangthai; }
            set { trangthai = value; }
        }
    }
    public class Cate
    {
        public Cate(int _maloai, string _tenloai)
        {
            this.maloai = _maloai;
            this.tenloai = _tenloai;
        }
        public Cate(DataRow row)
        {
            this.maloai = (int)row["maloai"];
            this.tenloai = row["tenloai"].ToString();
        }
        private int maloai;
        private string tenloai;
        public int Maloai
        {
            get { return maloai; }
            set { maloai = value; }
        }
        public string Tenloai
        {
            get { return tenloai; }
            set { tenloai = value; }
        }
    }
    public class ThucDon
    {
        public ThucDon(int _mamon, string _tenmon, int _maloai, Double _dongia, string _donvitinh)
        {
            this.mamon = _mamon;
            this.tenmon = _tenmon;
            this.maloai = _maloai;
            this.dongia = _dongia;
            this.donvitinh = _donvitinh;
        }
        public ThucDon(DataRow row)
        {
            this.mamon = (int)row["mamon"];
            this.tenmon = row["tenmon"].ToString();
            this.maloai = (int)row["maloai"];
            this.dongia = Convert.ToDouble(row["dongia"].ToString());
            this.donvitinh = row["donvitinh"].ToString();
        }
        private int mamon;
        private string tenmon;
        private int maloai;
        private Double dongia;
        private string donvitinh;
        public int Mamon
        {
            get { return mamon; }
            set { mamon = value; }
        }
        public int Maloai
        {
            get { return maloai; }
            set { maloai = value; }
        }
        public string Tenmon
        {
            get { return tenmon; }
            set { tenmon = value; }
        }
        public Double Dongia
        {
            get { return dongia; }
            set { dongia = value; }
        }
        public string Donvitinh
        {
            get { return donvitinh; }
            set { donvitinh = value; }
        }
    }
    public class HoaDon
    {
        public HoaDon(int _mahoadon, double? _giamgia, int _maban, DateTime? _gioden, double _tongtien, int _trangthai)
        {
            this.mahoadon = _mahoadon;
            this.giamgia = _giamgia;
            this.maban = _maban;
            this.gioden = _gioden;
            this.tongtien = _tongtien;
            this.trangthai = _trangthai;
        }
        public HoaDon(DataRow row)
        {
            this.mahoadon = (int)row["mahoadon"];
            this.giamgia = Convert.ToDouble(row["giamgia"].ToString());
            this.maban = (int)row["maban"];
            this.gioden = Convert.ToDateTime(row["gioden"].ToString());
            this.tongtien = Convert.ToDouble(row["tongtien"].ToString());
            this.trangthai = Convert.ToInt32(row["trangthai"].ToString());
        }
        private int mahoadon;
        private double? giamgia;
        private int maban;
        private DateTime? gioden;
        private double tongtien;
        private int trangthai;
        public int Mahoadon
        {
            get { return mahoadon; }
            set { mahoadon = value; }
        }
        public double? Giamgia
        {
            get { return giamgia; }
            set { giamgia = value; }
        }
        public int Maban
        {
            get { return maban; }
            set { maban = value; }
        }
        public DateTime? Gioden
        {
            get { return gioden; }
            set { gioden = value; }
        }
        public Double Tongtien
        {
            get { return tongtien; }
            set { tongtien = value; }
        }
        public int Trangthai
        {
            get { return trangthai; }
            set { trangthai = value; }
        }
    }
    public class Chitiet
    {
        public Chitiet(int _machitiethoadon, int _mahoadon, int _mamon, int _soluong, double _gia)
        {
            this.machitiethoadon = _machitiethoadon;
            this.mahoadon = _mahoadon;
            this.mamon = _mamon;
            this.soluong = _soluong;
            this.gia = _gia;
        }
        public Chitiet(DataRow row)
        {
            this.machitiethoadon = (int)row["machitiethoadon"];
            this.mahoadon = (int)row["mahoadon"];
            this.mamon = (int)row["mamon"];
            this.soluong = Convert.ToInt32(row["soluong"].ToString());
            this.gia = Convert.ToDouble(row["gia"].ToString());
        }
        private int machitiethoadon;
        private int mahoadon;
        private int mamon;
        private int soluong;
        private double gia;
        public int Machitiethoadon
        {
            get { return machitiethoadon; }
            set { machitiethoadon = value; }
        }
        public int Mahoadon
        {
            get { return mahoadon; }
            set { mahoadon = value; }
        }
        public int Mamon
        {
            get { return mamon; }
            set { mamon = value; }
        }
        public int Soluong
        {
            get { return soluong; }
            set { soluong = value; }
        }
        public Double Gia
        {
            get { return gia; }
            set { gia = value; }
        }
    }
}
