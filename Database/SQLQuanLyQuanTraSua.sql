﻿CREATE DATABASE Quan_Ly_Quan_Tra_Sua
go

use Quan_Ly_Quan_Tra_Sua
go

-- table chitiethoadon
-- table thucdon
-- table nhommon
-- table hoadon
-- table ban
-- table taikhoan

create table tbl_taikhoan
(
	tentaikhoan nvarchar(30) not null primary key,
	matkhau nvarchar(30) not null,
	quyen int not null default 0,
)
go

create table tbl_nhommon
(
	maloai int primary key not null IDENTITY(1,1),
	tenloai nvarchar(50) not null,
)
go

create table tbl_ban
(
	maban int primary key  IDENTITY(1,1),
	tenban nvarchar(50) not null,
	trangthai int default 0,
)
go

create table tbl_thucdon
(
	mamon int primary key  IDENTITY(1,1),
	tenmon nvarchar(50) not null,
	maloai int foreign key references tbl_nhommon(maloai),
	dongia float not null,
	donvitinh varchar(50),
)
go

create table tbl_hoadon
(
	mahoadon int primary key  IDENTITY(1,1),
	giamgia float,
	maban int foreign key references tbl_ban(maban),
	gioden datetime default getdate(),
	tongtien float,
	trangthai int,
)
go

create table tbl_chitiethoadon
(
	machitiethoadon int primary key IDENTITY(1,1),
	mahoadon int foreign key references tbl_hoadon(mahoadon),
	mamon int foreign key references tbl_thucdon(mamon),
	soluong int not null,
	gia float not null,
)
go

insert into tbl_taikhoan(tentaikhoan,matkhau,quyen)
values(N'chinhhi',N'123',0)
insert into tbl_taikhoan(tentaikhoan,matkhau,quyen)
values(N'dolinh',N'123',0)
insert into tbl_taikhoan
values(N'nhanvien',N'123',1)
go

insert into tbl_nhommon(tenloai)
values(N'Trà sữa trân châu')
insert into tbl_nhommon(tenloai)
values(N'Trà sữa truyền thống')
go
select * from tbl_nhommon
go

declare @i int = 1
while @i <= 10
begin
	insert into tbl_ban(tenban,trangthai)
	values(N'Bàn '+cast(@i as nvarchar(100)),0)
	set @i = @i+1
end
go
select * from tbl_ban
go

insert into tbl_thucdon(tenmon,maloai,dongia,donvitinh)
values(N'Trà sữa trân châu đen',1,20000,'ly')
insert into tbl_thucdon(tenmon,maloai,dongia,donvitinh)
values(N'Trà sữa trân châu trắng',2,20000,'ly')
select * from tbl_thucdon

insert into tbl_hoadon(giamgia,maban,gioden,tongtien,trangthai)
values(0,1,convert(datetime,'18-06-12 10:34:09 PM',5),0,0)
go

delete from tbl_chitiethoadon
DELETE FROM tbl_hoadon
delete from tbl_ban

select * from tbl_ban
select * from tbl_hoadon where trangthai = 0
select * from tbl_chitiethoadon where mahoadon = 35

UPDATE T
   SET C1 = 1
 WHERE C2 = 'a'

 CREATE OR ALTER function groupbymaHD(@mahoadon int)
 returns nvarchar(1000)
 as
	begin
		declare @res nvarchar(1000) = ''
		select @res = @res + b.tenmon + '(' + CONVERT(varchar,a.soluong) + '), '
		from tbl_chitiethoadon a,tbl_thucdon b
		where a.mahoadon = @mahoadon and a.mamon = b.mamon
		return @res
	end
go

select dbo.groupbymaHD(145) as 'Các món'

CREATE OR ALTER proc thongketheohoadon @ngayBD date, @ngayKT date
as
	begin
		select a.mahoadon as N'Mã hóa đơn', a.gioden as N'Thời gian', a.tongtien - a.tongtien*(a.giamgia/100.0) as N'Tiền món', a.giamgia as N'Giảm giá(%)', a.tongtien as N'Thành tiền',b.tenban as N'Điểm bán', dbo.groupbymaHD(a.mahoadon) as N'Các món'
		from tbl_hoadon a,tbl_ban b
		where a.maban = b.maban and a.gioden between @ngayBD and @ngayKT and a.trangthai = 1
	end
go

exec thongketheohoadon '2000-1-1','2020-1-1'

select * from tbl_hoadon
where trangthai = 1

CREATE OR ALTER proc thongketheomon @ngayBD date, @ngayKT date
as
	begin

		select c.tenmon as N'Tên món', sum(b.soluong) as 'Số lượng', sum(b.soluong*c.dongia) as 'Doanh thu'
		from tbl_hoadon a, tbl_chitiethoadon b, tbl_thucdon c
		where a.trangthai = 1 and a.gioden between @ngayBD and @ngayKT and a.mahoadon = b.mahoadon and b.mamon = c.mamon
		group by c.tenmon
	end
go

select * from tbl_chitiethoadon
go
select * from tbl_hoadon
go
exec thongketheomon '2000-1-1','2020-1-1'

select count(tentaikhoan) from tbl_taikhoan